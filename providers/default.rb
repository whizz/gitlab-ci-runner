#
# Copyright (c) 2015 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

use_inline_resources
env = ''

action :register do
 if runner_token(new_resource.description, new_resource.config).nil?
    options = []
    new_resource.attributes.each do |array|
      key = array.first
      value = new_resource.send(key)
      keyname = key.to_s.tr('_','-')
      keyname = 'registration-token' if keyname == 'token'
      options << "--#{keyname} '#{value}'" unless value.nil?
    end
    options = options.join(' ')
    if new_resource.unset_proxy
      env = "#{env} http_proxy='' https_proxy='' HTTP_PROXY='' HTTPS_PROXY=''"
    end

    execute "#{env} gitlab-runner register --non-interactive #{options}" do
      user 'gitlab-runner'
      cwd '/home/gitlab-runner'
    end
    new_resource.updated_by_last_action(true)
  end
end

action :unregister do
  if new_resource.unset_proxy
    env = "#{env} http_proxy='' https_proxy='' HTTP_PROXY='' HTTPS_PROXY=''"
  end

  token, url = runner_token(new_resource.description, new_resource.config)
  if !url.nil?
    execute "#{env} gitlab-runner unregister --token #{token} --url #{url}" do
      user 'gitlab-runner'
      cwd '/home/gitlab-runner'
    end
    new_resource.updated_by_last_action(true)
  end
end

def toml_parse(file)
  chef_gem 'toml' do
    compile_time true
  end
  require 'toml'
  toml = {}
  toml = ::TOML.load_file(file) if ::File.file? file
  toml['runners'] ||= []
  toml
end

def runner_token(description, config_file)
  config = toml_parse config_file
  config['runners'].reverse.each do |r|
    return [r['token'], r['url']] if r['name'] == description
  end
  nil
end
