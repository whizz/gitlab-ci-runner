Gitlab CI Runner
================

Description
-----------

Install and configure *runners* for **gitlab-ci**. Use
**gitlab-ci-multi-runner** as runner type. Learn more on
<https://gitlab.com/gitlab-org/gitlab-ci-multi-runner>.

Usage
-----

### Easy Setup

Add `recipe[gitlab-ci-runner]` in your run-list to install
**gitlab-ci-multi-runner** using its repository.

To add runners, use default provider or use
`recipe[gitlab-ci-runner::register]` and `runners` attribute. To see a use
case, look at [.kitchen.yml](.kitchen.yml).

### Test

This cookbook is fully tested through the installation of the full gitlab
platform in docker hosts. This uses kitchen, docker and some monkey-patching.

If you run kitchen list, you will see 3 suites, Each corresponds to a different
server:

- dnsdock-centos-7: DNS for docker
- gitlab-centos-7: full installation of **gitlab** and **gitlab-ci**
- runner-centos-7: installation of **gitlab-ci-multi-runner** and configuration
  of runners

For more information, see [.kitchen.yml](.kitchen.yml) and [test](test)
directory.

Changes
-------

### 1.0.0:

- Initial version with Centos 7 support

Requirements
------------

### Cookbooks

Declared in [metadata.rb](metadata.rb).

### Gems

Declared in [Gemfile](Gemfile).

### Platforms

A *systemd* managed distribution:
- RHEL Family 7, tested on Centos

Note: it should work fine on Debian 8 but the official docker image does not
allow systemd to work easily, so it could not be tested.

Attributes
----------

Configuration is done by overriding default attributes. All configuration keys
have a default defined in [attributes/default.rb](attributes/default.rb).
Please read it to have a comprehensive view of what and how you can configure
this cookbook behavior.

Recipes
-------

### default

Install **gitlab-ci-multi-runner** by using its repository, enable and start
the service.

### repository

Install runner repository.

### package

Install **gitlab-ci-multi-runner** package.

### service

Enable and start runner service.

### register

Register or Unregister runners based on attribute 'runners'. See
[.kitchen.yml](.kitchen.yml) for more information.

Resources/Providers
-------------------

### default

Register or Unregister runners. For instance, to register a default runner
to http://gitlab-ci.myinstance:

```ruby
gitlab_ci_runner 'my runner' do
  token '1234567890'
  url 'http://gitlab-ci.myinstance'
end
```

Contributing
------------

You are more than welcome to submit issues and merge requests to this project.

### Commits

Your commits must pass `git log --check` and messages should be formated
like this (based on this excellent
[post](http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html)):

```
Summarize change in 50 characters or less

Provide more detail after the first line. Leave one blank line below the
summary and wrap all lines at 72 characters or less.

If the change fixes an issue, leave another blank line after the final
paragraph and indicate which issue is fixed in the specific format
below.

Fix #42
```

Also do your best to factor commits appropriately, ie not too large with
unrelated things in the same commit, and not too small with the same small
change applied N times in N different commits. If there was some accidental
reformatting or whitespace changes during the course of your commits, please
rebase them away before submitting the MR.

### Files

All files must be 80 columns width formatted (actually 79), exception when it
is not possible.

License and Author
------------------

- Author:: Samuel Bernard (<samuel.bernard@s4m.io>)

```text
Copyright:: 2015, Sam4Mobile

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
