name 'test-gitlab-ci-runner'
maintainer 'Sam4Mobile'
maintainer_email 'samuel.bernard@s4m.io'
license 'Apache 2.0'
description 'Test Gitlab CI Runner'
long_description 'Test Gitlab CI Runner'
source_url 'https://gitlab.com/s4m-chef-repositories/gitlab-ci-runner'
issues_url 'https://gitlab.com/s4m-chef-repositories/gitlab-ci-runner/issues'
version '1.0.0'

supports 'centos',  '>= 7.1'

depends 'gitlab-ci-runner'
