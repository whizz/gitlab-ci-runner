#
# Copyright (c) 2015 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

runners = node['gitlab-ci-runner']['runners']

runners.each do |runner|
  resource = Chef::Resource::GitlabCiRunner.new(
    runner['description'], run_context
  )
  resource.cookbook_name= cookbook_name
  resource.recipe_name= recipe_name

  runner.each do |name, value|
    resource.send("#{name}", value)
  end

  action = runner['action']
  action = :register if action.nil?
  ruby_block 'run gitlab-ci-runner resource' do
    block do
      resource.run_action action
    end
  end
end
