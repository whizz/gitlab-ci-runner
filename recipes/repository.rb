#
# Copyright (c) 2015 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

case node['platform_family']
when 'rhel'
  include_recipe 'yum'
#  package 'pygpgme'
  yum_repository 'gitlab-ci-runner' do
    description 'GitLab CI Runner'
    baseurl 'https://packages.gitlab.com/runner/gitlab-ci-multi-runner/el/' \
      "#{node['platform_version'].split('.').first}/$basearch"
    gpgkey 'https://packages.gitlab.com/gpg.key'
    gpgcheck false
    #options(repo_gpgcheck: true)
  end
when 'debian'
  apt_repository 'gitlab-ci-runner' do
    uri 'https://packages.gitlab.com/runner/gitlab-ci-multi-runner/' \
      "#{node['platform']}/"
    distribution node['lsb']['codename']
    components ['main']
    key 'https://packages.gitlab.com/gpg.key'
  end
end
