#
# Copyright (c) 2015 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

actions :register, :unregister
default_action :register

attributes = [
  [ :description, :kind_of => String, :name_attribute => true ],
  [ :url, :kind_of => String, :default => 'https://ci.gitlab.com/' ],
  [ :executor, :kind_of => String, :default => nil ],
  [ :limit, :kind_of => Integer, :default => nil ],
  [ :config,  :kind_of => String, :default =>
     '/home/gitlab-runner/.gitlab-runner/config.toml' ],
  [ :token, :kind_of => String, :default => nil ],
  [ :tag_list, :kind_of => Array, :default => nil ],

  [ :docker_image, :kind_of => String, :default => nil ],
  [ :docker_priviledged, :kind_of => String, :default => nil ],
  [ :docker_mysql, :kind_of => String, :default => nil ],
  [ :docker_postgres, :kind_of => String, :default => nil ],
  [ :docker_mongo, :kind_of => String, :default => nil ],
  [ :docker_redis, :kind_of => String, :default => nil ],

  [ :parallels_vm, :kind_of => String, :default => nil ],

  [ :ssh_host, :kind_of => String, :default => nil ],
  [ :ssh_port, :kind_of => String, :default => nil ],
  [ :ssh_user, :kind_of => String, :default => nil ],
  [ :ssh_password, :kind_of => String, :default => nil ],
  [ :ssh_identity_file, :kind_of => String, :default => nil ]
]

attributes.each do |attr|
  attribute *attr
end

attribute :attributes, :kind_of => Array, :default => attributes
attribute :unset_proxy, :kind_of => [TrueClass, FalseClass], :default => false
